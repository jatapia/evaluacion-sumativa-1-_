import sqlite3
import os

#EstablecerConexionBaseDatos
conex = sqlite3.connect("GuettoTime")
cursor = conex.cursor()
#Sentencia = "CREATE TABLE PalabrasGuetto (" \
#            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " \
#            "palabrasIncorreta VARCHAR(30), " \
#            "palabrasCorrectas VARCHAR(30))"
#cursor.execute(Sentencia)
#conex.close()



def inicioMenu():
    os.system('cls')
    print("<<<<--------- SELECCIONE UNA OPCIÓN --------->>>>")
    print("\t 1- Agregar nueva palabra")
    print("\t 2- Editar una palabra")
    print("\t 3- Ver listado completo")
    print("\t 4- Buscar significado")
    print("\t 5- Eliminar Registro")
    print("\t 9- Salir")


#OPCION 1 AGREGAR PALABRA
def agregarPalabra():
    os.system('cls')
    NuevaPalabra = input("Ingresar nueva palabra del ghetto: ")
    print(NuevaPalabra)
    i = 0
    signiIncorrecto = ["xopa", "awebao", "ahuevao", "mopri", "llesca", "yesca", "chuzo", "compa","chambon","bulto","birria",
                       "chaniao", "tongo", "guabanazo","biencuidao","rakataca", "chacalita","takillar", "taquillar",
                       "rantan", "liso","lisa", "ponchera ","vaina", "chiquishow", "cocoa","cabreas", "joder", "pollo", "polla"]

    signiCorrecto = ["Que paso", "Estas pendejo", "vas como el huevo", "amigo o pasiero","vamos a salir", "vamos a salir",
                     "me acorde de algo", "compadre","no tiene muchas habilidades","falta de habilidades",
                     "jugar","bien vestido", "policia", "golpe", "te cuido el carro", "chica de barrio","chica de barrio",
                     "ganar popularidad", "ganar popularidad", "hay mucho de algo", "atrevido","atrevida", "algo atrevido",
                     "alguna cosa", "hacer un espectaculo", "noticia importante","molestas","molestar","hombre","mujer"]
    Auxiliar = ""
    for i in range(len(signiIncorrecto)):
        if  NuevaPalabra.lower() ==  signiIncorrecto[i]:
            Auxiliar = signiCorrecto[i]
            #print("si se encontro")
            #print(Auxiliar)
            break
        else:
            print("")
    i += 1
    datos = (NuevaPalabra,Auxiliar)
    insertarSql = """INSERT INTO PalabrasGuetto (palabrasIncorreta,palabrasCorrectas) VALUES (?,?)"""

    if (cursor.execute(insertarSql,datos)):
        print("Datos guardados correctamente")
    else:
        print("Error al guardar los datos")

    conex.commit()
#    conex.close()


#OPCION 2 EDITAR PALABRA
def editarListado():
    os.system('cls')
    cursor.execute("SELECT id,palabrasIncorreta  FROM PalabrasGuetto")
    print("")
    print("\t id \t Palabras incorrectas")
    for x in cursor:
        print('\t '+str(x[0])+'\t\t\t'+str(x[1]))

    idSql = input("Digite numero de id a modificar:  \t")

    for e in cursor:
        print(e[0])
        if int(e[0] == int(idSql)):
            Palabraincorrecta = e[1]
            break
    NuevaPalabra = input("Ingresar nueva palabra del ghetto: \t")
    i = 0
    signiIncorrecto = ["xopa", "awebao", "ahuevao", "mopri", "llesca", "yesca", "chuzo", "compa", "chambon", "bulto",
                       "birria",
                       "chaniao", "tongo", "guabanazo", "biencuidao", "rakataca", "chacalita", "takillar", "taquillar",
                       "rantan", "liso", "lisa", "ponchera ", "vaina", "chiquishow", "cocoa", "cabreas", "joder",
                       "pollo", "polla"]

    signiCorrecto = ["Que paso", "Estas pendejo", "vas como el huevo", "amigo o pasiero", "vamos a salir",
                     "vamos a salir",
                     "me acorde de algo", "compadre", "no tiene muchas habilidades", "falta de habilidades",
                     "jugar", "bien vestido", "policia", "golpe", "te cuido el carro", "chica de barrio",
                     "chica de barrio",
                     "ganar popularidad", "ganar popularidad", "hay mucho de algo", "atrevido", "atrevida",
                     "algo atrevido",
                     "alguna cosa", "hacer un espectaculo", "noticia importante", "molestas", "molestar", "hombre",
                     "mujer"]
    Auxiliar = ""
    for i in range(len(signiIncorrecto)):
        if NuevaPalabra.lower() == signiIncorrecto[i]:
            Auxiliar = signiCorrecto[i]
            # print("si se encontro")
            # print(Auxiliar)
            break
        else:
            print("no se encontro")
    i += 1

    editar_sql = "UPDATE PalabrasGuetto SET palabrasIncorreta ='"+NuevaPalabra+"',palabrasCorrectas ='"+Auxiliar+"'WHERE ID ="+idSql
    cursor.execute(editar_sql)
    conex.commit()
#    conex.close()


#OPCION 3 MOSTRAR LAS PALABRAS
def mostrarListado():
    os.system('cls')
    cursor.execute("SELECT id, palabrasIncorreta FROM PalabrasGuetto")
    print("Palabras Agregadas")
    print("\t id \t Palabras incorrectas")
    for x in cursor:
        print('\t '+str(x[0])+'\t\t\t'+str(x[1]))
    conex.commit()
#    conex.close()


def buscarSignificado():
    os.system('cls')
    cursor.execute("SELECT id, palabrasIncorreta FROM PalabrasGuetto")
    print("Palabras Agregadas")
    print("\t id \t Palabras incorrectas")
    for x in cursor:
        print('\t '+str(x[0])+'\t\t\t'+str(x[1]))

    buscarSigId = input("Digite el id que desea saber su significado: \t ")
    SignicadoSql = ("SELECT * FROM PalabrasGuetto WHERE id ="+buscarSigId)
    cursor.execute(SignicadoSql)

    print("\t id \t Palabras incorrectas \t Palabras Correctas")
    for mostrar in cursor:
        print('\t '+str(mostrar[0])+'\t\t\t'+str(mostrar[1])+'\t\t\t'+str(mostrar[2]))


    conex.commit()
 #   conex.close()



def seguir(num):
    os.system('cls')
    print("Desea continuar...")
    print("1. Si")
    print("2. No")
    cont = int(input("Escriba su respuesta:  \t"))
    if (cont == 1):
        num = 0
        print("entro en el 1")
    else:
        num = 1
        print("entra en el 2")
    return num


#OPCION 5 ELIMINAR PALABRA
def eliminarPalabra():
    os.system('cls')
    cursor.execute("SELECT id, palabrasIncorreta FROM PalabrasGuetto")
    print("Palabras Agregadas")
    print("\t id \t Palabras incorrectas \t Palabras Correctas")
    for x in cursor:
        print('\t '+str(x[0])+'\t\t\t'+str(x[1]))

    EliminarId = input("Digite el id de la palabra a eliminar: \t ")
    DeleteSql = ("DELETE FROM PalabrasGuetto WHERE id ="+EliminarId)

    cursor.execute(DeleteSql)
    conex.commit()
#    conex.close()


num = 0
while num != 1:
    inicioMenu()
    ElegirOpcion = int(input("<< Elija una opcion: >> \t"))

    if ElegirOpcion == 1:
        print("Elegiste Agregar una nueva palabra \t")
        agregarPalabra()
    elif ElegirOpcion == 2:
            print("Elegiste editar")
            editarListado()
    elif ElegirOpcion == 3:
            print("Elegiste ver listado")
            mostrarListado()
    elif ElegirOpcion == 4:
            print("Buscar significado")
            buscarSignificado()
    elif ElegirOpcion == 5:
            print("Eliminar  Palabra")
            eliminarPalabra()
    elif ElegirOpcion == 9:
        conex.close()
        break
    else:
        print("")
        input("No has pulsado ninguna opción correcta...\n pulsa una tecla para continuar")
